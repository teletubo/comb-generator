package xnum;

import java.io.FileOutputStream;
import java.util.LinkedList;

public class MyCombinationsSaver { 
	String fileName;
	public int garantir, drawSize, numbersSize;
	LinkedList<int[]> allCombs = new LinkedList<int[]>();
	
	 	
	private void generateOutputFilename()
	{ 
		
		String sDrawSize =drawSize+"";
		
		String sGuarantees = garantir+"";
		
		String sNumbers = numbersSize+"";

		System.out.println("drawSize:"+sDrawSize+"  -  Guarantees:"+sGuarantees+"  -  Numbers: "+sNumbers+ " qty:"+allCombs.size());
		
		
		int numbers = Integer.parseInt(sNumbers);
		
		int guarantees = Integer.parseInt(sGuarantees);
		
		
		int drawSize = Integer.parseInt(sDrawSize);
		
		fileName = String.format("%02d", numbers) + "_" + String.format("%02d", drawSize) + "_"+String.format("%02d", guarantees)+".cmb";
		
		System.out.println("filename: "+fileName);
	 
		
		
		
		
	}
	
	public MyCombinationsSaver(int garantir, int drawSize, int numbersSize,
			LinkedList<int[]> allCombs) {
		super(); 
		this.garantir = garantir;
		this.drawSize = drawSize;
		this.numbersSize = numbersSize;
		this.allCombs = allCombs;
	}

	public void loadFile(String filePath)
	{
		
	}
	
	public String saveFile( ) throws Exception 
	{
		return saveFile(null);
	}
	public String saveFile(String path) throws Exception 
	{
		generateOutputFilename(); 
		
		if (path!=null)
		{
			fileName = path+"/"+fileName;
		}
		
		FileOutputStream fos = new FileOutputStream(fileName);
		
		 

		for (int i=0;i<allCombs.size();i++)
		{
		  int comb[] = allCombs.get(i); 
			  for (int k=0;k<comb.length;k++)
			  { 
				  fos.write(comb[k]);
				  
				  //System.out.print(Integer.parseInt(nums[i])+" ");
			  }
			   
		
		} 
		
		fos.close();
		
		return fileName;
		
		
	}
}
