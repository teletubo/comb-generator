package xnum;

import java.util.Arrays;
import java.util.HashMap;
import java.util.LinkedList;

public class BruteForceCombinations2 {



	int[] arr = {1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20};
	//int[] arr = {1,2,3,4,5,6,7,8,9,10,11,12};
	int[] ctnums = new int[arr.length];
	int fixedNumbers=2;
	static int garantir=4;
	int size = 6;

	public static void main(String[] args){
		BruteForceCombinations2 c = new BruteForceCombinations2(); 

		System.out.println("combs:"+c.getNCombs(  garantir,c.size));
		//c.preLoad();

		c.generate();

		//c.removeMoreThanCombinations();
//		c.allCombs = new Munir().asLinkedList();
//				for (int i=0;i<1;i++)
//				{   int size = c.allCombs.size()-1; 
//					//c.allCombs.remove((int)(Math.random()*size)); 
//				}

	
		c.printCombs();

		//c.test(new int[]{1, 2, 3, 4, 5, 7}, garantir); 

		c.testAll();
	}


	
	public void generate()
	{
		while (true)
		{
			failedTests=0;
			createallCombinations(arr, size, 0, new int[size]);
			System.out.println("failed tests:"+failedTests);
			if (failedTests==0) break;
		}
	}






	private int getArrayPos(int num)
	{
		for (int i=0;i<arr.length;i++)
		{
			if (arr[i]==num) return i;
		}
		return -1;
	}

	/**
	 * pega todos as combinacao entre os 20 numeros escolhidos
	 * @param arr
	 * @param len
	 * @param startPosition
	 * @param result
	 */
	private void volanteCombinations(int[] arr, int len, int startPosition, int[] result,int countShould){

		if (len == 0){ 

			int jogo[] = result.clone(); //jogo de 6 numeros. 

			//com este jogo, validar todas as quadras que ele cobre
			tempGames.clear();
			covering(jogo, garantir, 0, new int[garantir]);

			int ct =0;
			for (int i=0;i<tempGames.size();i++)
			{
				MinGameInstance mgi = tempGames.get(i);
				MinGameInstance mgFromAll = games.get(mgi);

				if (!mgFromAll.covered) ct++; //a game that has not been covered is now covered

			} 
			if (countShould==ct)
			{
				allCombs.add(jogo);
				gamesFound++;
				for (int i=0;i<tempGames.size();i++)
				{
					MinGameInstance mgi = tempGames.get(i);
					MinGameInstance mgFromAll = games.get(mgi);
					mgFromAll.covered=true; //mark as covered
				}

				//skippar todos os jogos que nao precisam de quadra pra ganhar




			}

			return;
		}       
		for (int i = startPosition; i <= arr.length-len; i++){
			result[result.length - len] = arr[i];
			volanteCombinations(arr, len-1, i+1, result,countShould);
		}
	}     

	LinkedList<MinGameInstance> tempGames=new LinkedList<>();

	private void covering(int[] arr, int len, int startPosition, int[] result){
		if (len == 0){
			MinGameInstance mgi = new MinGameInstance();
			mgi.numbers=result.clone();
			mgi.covered=false;
			tempGames.add(mgi);
			return;
		}       
		for (int i = startPosition; i <= arr.length-len; i++){
			result[result.length - len] = arr[i];
			covering(arr, len-1, i+1, result);
		}
	}



	LinkedList<int[]> allCombs = new LinkedList<int[]>();



	static int testNum=0;


	private boolean testCt(int[] sorteados, int garantir)
	{

		testNum++;
		boolean found=false;
		for (int i=0;i<allCombs.size();i++)
		{
			int test[] = allCombs.get(i);
			int ct=0;
			for (int k=0;k<sorteados.length;k++)
			{
				for (int j=0;j<sorteados.length;j++)
					if (sorteados[k]==test[j])
						ct++;
			}

			if (ct>garantir)
			{ 
				found=true;
			}
		}

		return found;

	}

	private boolean test(int[] sorteados, int garantir)
	{

		testNum++;
		boolean found=false;
		for (int i=0;i<allCombs.size();i++)
		{
			int test[] = allCombs.get(i);
			int ct=0;
			for (int k=0;k<sorteados.length;k++)
			{
				for (int j=0;j<sorteados.length;j++)
					if (sorteados[k]==test[j])
						ct++;
			}

			if (ct>=garantir)
			{

				//System.out.println(testNum+":Achou result("+ct+"): "+Arrays.toString(test));
				found=true;
			}
		}

		return found;

	}


	private void printCombs()
	{
		for (int i=0;i<allCombs.size();i++)
		{
			System.out.println((i+1)+":"+Arrays.toString(allCombs.get(i)));
		}
	}



	private void testAll()
	{
		allCombinations(arr, size, 0, new int[size]);
	}



	int failedTests=0;
	private void allCombinations(int[] arr, int len, int startPosition, int[] result){
		if (len == 0){
			if (!test(result,garantir))
			{
				failedTests++;
				System.out.println(failedTests+"=="+Arrays.toString(result)+" failed");
			}
			return;
		}       
		for (int i = startPosition; i <= arr.length-len; i++){
			result[result.length - len] = arr[i];
			allCombinations(arr, len-1, i+1, result);
		}
	}


	private void createallCombinations(int[] arr, int len, int startPosition, int[] result){
		if (len == 0){
			if (!test(result,garantir))
			{


				System.out.println("Testing "+Arrays.toString(result));
				failedTests++;
				allCombs.add(result.clone());
				//System.out.println("allcombsuze:"+allCombs.size());
			}
			return;
		}       
		for (int i = startPosition; i <= arr.length-len; i++){
			result[result.length - len] = arr[i];
			createallCombinations(arr, len-1, i+1, result);
		}
	}


	private void preLoad(){
		for (int i=0;i<arr.length-size;i+=size)
		{
			int game[] = new int[size];
			for (int k=0;k<size;k++)
			{
				game[k]=arr[i+k];
			}
			allCombs.add(game);
		}
	}




	private void gameCombinations(int[] arr, int len, int startPosition, int[] result){
		if (len == 0){
			MinGameInstance mgi = new MinGameInstance();
			mgi.numbers=result.clone();
			mgi.covered=false;
			games.put(mgi,mgi);
			return;
		}       
		for (int i = startPosition; i <= arr.length-len; i++){
			result[result.length - len] = arr[i];
			gameCombinations(arr, len-1, i+1, result);
		}
	}





	private HashMap<MinGameInstance,MinGameInstance> games = new HashMap<>();
	private HashMap<MinGameInstance,MinGameInstance> skipGames = new HashMap<>();
	private void generateAllGamesCombinations()
	{ 
		gameCombinations(arr, garantir, 0, new int[garantir]); 
		System.out.println("games:"+games.size());


		for (int i=1;i<=getNCombs(  garantir,size);i++)
		{
			gamesFound=0;
			volanteCombinations(arr, size, 0, new int[size],i);
			System.out.println("Games taht cover"+i+" found:"+gamesFound);
		} 



	}


	private int getNCombs(int r, int n)
	{
		return   factorial(n)/(factorial(r)*factorial(n-r)) ;
	}


	private int      factorial(int n) {
		int fact = 1; // this  will be the result
		for (int i = 1; i <= n; i++) {
			fact *= i;
		}
		return fact;
	}
	static int gamesFound=0;


}