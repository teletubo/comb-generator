package xnum;

import java.io.File;

import javax.management.RuntimeErrorException;

import xnum.randomcombinator.GoodCombinationListener;

public class FileRandomCombinationGenerator implements GoodCombinationListener{


	public static void main(String[] args) throws Exception {
		
		if (args.length==0)
			throw new RuntimeErrorException(new Error("Please input a source"));
		FileRandomCombinationGenerator fcg = new FileRandomCombinationGenerator(args[0]);
		fcg.combine();
	}

	int drawSize = 15;
	RandomCombinations bc2;
	int garantir;
	int numbersSelected;
	String source;
	public FileRandomCombinationGenerator(String source)
	{
		this.source = source;

		File theDir = new File(getSourcePath()); 
		if (!theDir.exists()) {  
		    try{ theDir.mkdir();   } 
		    catch(SecurityException se){ }
		}


	}

	int ct=0;
	public void combine()
	{


		while(true)
		{
			for (numbersSelected=20;numbersSelected<25;numbersSelected++)
			{

				for (garantir=11;garantir<15;garantir++)
				{
					int maxSize = 9999999;
					CmbLoaderTest cmb = new CmbLoaderTest(); 
					CmbLoaderTest cmbBest = new CmbLoaderTest(); 
					try{
						cmb.load(getFileName(),getSourcePath());  
						
						cmbBest.load(getFileName(),"best");
						
						if (cmbBest.allCombs.size()>0 && cmb.allCombs.size()>0)
						{
							if (cmbBest.allCombs.size()< cmb.allCombs.size())
								maxSize = cmbBest.allCombs.size();
							else
								maxSize = cmb.allCombs.size();
						}
						else if (cmbBest.allCombs.size()>0)
						{
							maxSize = cmbBest.allCombs.size();
						}
						else if (cmb.allCombs.size()>0)
						{
							maxSize = cmb.allCombs.size();
						}
						
						 
						System.out.println("Setting max size for "+numbersSelected+" garantir:"+garantir+"  size:"+maxSize);
					}catch(Exception e)
					{
						e.printStackTrace();
					};

					bc2 = new RandomCombinations(drawSize, garantir, numbersSelected);
					bc2.maxGames=maxSize;
					bc2.setGoodCombinationListener(this);
					bc2.generate(); 
					System.out.println("Finished tries for "+numbersSelected+" garantir:"+garantir);
				}
			}
		}

	}


	private String getFileName()
	{
		return String.format("%02d", numbersSelected) + "_" + String.format("%02d", drawSize) + "_"+String.format("%02d", garantir)+".cmb";

	}

	@Override
	public void finishedCombination(boolean good) {

		ct++;
		if (good)
		{


			MyCombinationsSaver mcs = new MyCombinationsSaver( garantir, drawSize, numbersSelected, bc2.lastGoodComb);
			try {
				mcs.saveFile(getSourcePath());
			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}



		}
		if (ct>1000)
		{
			bc2.setRunning(false);
			ct=0;
		}
	}
	
	
	private String getSourcePath()
	{
		return "combs_"+source;
	}

}
