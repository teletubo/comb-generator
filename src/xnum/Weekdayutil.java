package xnum;

import java.util.Calendar;
import java.util.Date;

public class Weekdayutil {

	public static void main(String[] args) {

		Date d = new Date();
		
		
		Date nexTuesday = getNextDay(d, Calendar.TUESDAY);
		
		System.out.println("nexttu:"+nexTuesday);
		
		
		Date nexNextFriday = getNextDay(nexTuesday, Calendar.FRIDAY);
		
		System.out.println("nextfri:"+nexNextFriday);
		
	}

	
	
	
	public static Date getNextDay(Date fromDay, int whichWeekday)
	{

		Calendar now = Calendar.getInstance();
		now.setTime(fromDay); 
		while (true)
		{
			now.add(Calendar.DATE, 1);
			int weekday = now.get(Calendar.DAY_OF_WEEK);

			if (weekday == whichWeekday)
			{
				return now.getTime();
			}

		}

	}
}
