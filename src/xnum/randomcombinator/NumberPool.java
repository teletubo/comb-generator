package xnum.randomcombinator;

import java.util.Collections;
import java.util.LinkedList;

public class NumberPool {

	private int startNum, endNum;
	private int startSize, reductedSize,gameSize;
	int totalNums;
	int diffNumbersAvailable=0;
	private LinkedList<XNumber> allnums = new LinkedList<>();


	public NumberPool( int endNum, int startSize, int reductedSize,int gameSize) {
		super();
		this.startNum = 1;
		this.endNum = endNum;
		this.startSize = startSize;
		this.reductedSize = reductedSize;
		this.gameSize=gameSize;
		initializeMap(startSize);


	}
	
	public void reorder()
	{
		
		Collections.sort(allnums);
	}

	public int getNumber(int result[])
	{

		int tries = 100;
		while(true)
		{
			tries--;
			if (tries==0) 
				{
				System.out.println("Failed getting number!!");
				return -1;
				}
			int randomSize =(int)( allnums.size()/1.9);
			if (randomSize<=gameSize)
				randomSize = gameSize+1;
			
			int idx = (int)(Math.random()*gameSize);
			
	 

			XNumber xn = allnums.get(idx);

			if (xn.qty==0) continue; //already used all alotted numbers

			boolean alreadyHas = false;
			for (int i=0;i<result.length;i++)
			{
				if (xn.num==result[i])
				{
					alreadyHas=true;
					break;
				}
			}
			if (!alreadyHas) 
			{
				xn.qty--;
				totalNums--;
				if (xn.qty==0)
					diffNumbersAvailable--;
				
				
				if (totalNums<1.5*result.length||diffNumbersAvailable<result.length)
					initializeMap(startSize);
				
				return xn.num;
			}
		}



	}


	private void initializeMap(int qty)
	{
		//System.out.println("Reinitialize Map: "+totalNums+"  avaiable:"+diffNumbersAvailable);
		allnums.clear();
		totalNums=0;
		
		for (int i=startNum;i<=endNum;i++)
		{
			XNumber xn= new XNumber();
			xn.num = i;
			xn.qty=qty;
			totalNums = totalNums + qty;
			allnums.add(xn);
		}
		diffNumbersAvailable = allnums.size();
	}

}
