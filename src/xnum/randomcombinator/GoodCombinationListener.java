package xnum.randomcombinator;

public interface GoodCombinationListener {

	void finishedCombination(boolean good);
}
