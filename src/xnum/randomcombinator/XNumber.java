package xnum.randomcombinator;

public class XNumber implements Comparable<XNumber>{

	public int num, qty;

	@Override
	public int compareTo(XNumber o) {
		if (this.qty> o.qty ) return -1;
		if (this.qty< o.qty ) return 1;
		return 0;
	}
	
}
