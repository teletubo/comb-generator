package xnum;

import java.util.Arrays;
import java.util.LinkedList;

import xnum.randomcombinator.GoodCombinationListener;
import xnum.randomcombinator.NumberPool;

public class RandomCombinations {



	int[] arr = {1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20};
	//int[] arr = {1,2,3,4,5,6,7,8,9,10,11,12};
	int[] ctnums = new int[arr.length];
	int maxGames=9999999;
	static int garantir=4;
	int size = 6;
	NumberPool numberPool;
	LinkedList<int[]> allCombs = new LinkedList<int[]>();
	LinkedList<int[]> lastGoodComb = new LinkedList<int[]>();
	GoodCombinationListener listener;
	boolean running=true;
	boolean stopTesting = false;


	int testNum=0;
	int maxGoodTestNum=0;
	
	public boolean isRunning() {
		return running;
	}



	public void setRunning(boolean running) {
		this.running = running;
	}



	public static void main(String[] args){
		RandomCombinations c = new RandomCombinations(6,4,20); 




		c.generate();

		//c.removeMoreThanCombinations();
		//		c.allCombs = new Munir().asLinkedList();
		//				for (int i=0;i<1;i++)
		//				{   int size = c.allCombs.size()-1; 
		//					//c.allCombs.remove((int)(Math.random()*size)); 
		//				}


		c.printCombs();

		//c.test(new int[]{1, 2, 3, 4, 5, 7}, garantir); 

		c.testAll(true);
	}



	public RandomCombinations(int psize, int pgarantir, int pPoolSize)
	{

		this.size=psize;
		garantir=pgarantir;

		arr = new int[pPoolSize];
		ctnums = new int[arr.length];
		for (int i=1;i<=pPoolSize;i++)
			arr[i-1]=i;
		maxGames = pPoolSize*pgarantir*psize;
		numberPool = new NumberPool(arr[arr.length-1], 20, 10,psize);
	}

	
 
	
	
	public void generate()
	{


		while(running)
		{ 
			failedTests=-1;
			allCombs.clear();
			int prevMaxGood=-111;
			maxGoodTestNum=0;
			while (true)
			{
				failedTests=0;
				addNumber();
				prevMaxGood = maxGoodTestNum;
				testAll(false);

				//System.out.println("failed tests:"+failedTests+" maxtestnum:"+maxGoodTestNum+" size:"+allCombs.size());
 
				if (maxGoodTestNum==prevMaxGood)
					allCombs.removeLast();
				
				
				if (failedTests==0 ||allCombs.size()>=maxGames) break;
			}//while true interno

			if (failedTests==0)
			{
				if (allCombs.size()<maxGames)
				{
					System.out.println("Found games, size: "+allCombs.size());
					lastGoodComb.clear();
					lastGoodComb.addAll(allCombs);
					if (listener!=null)
						{
						listener.finishedCombination(true);
						maxGames = allCombs.size();
						}
				}
				else
				{
					if (listener!=null)
						listener.finishedCombination(false);
					
					//System.out.println("Found same size.");
				}
			}
			else
			{
				
				if (listener!=null)
					listener.finishedCombination(false);
				//System.out.println("Retries Exceeded and failed "+failedTests+" tests.");
			}


		}//while true externo

	}











 

	private boolean test(int[] sorteados, int garantir)
	{

		testNum++;
		
		if (testNum < maxGoodTestNum)
			return true;
		
		boolean found=false;
		for (int i=0;i<allCombs.size();i++)
		{
			int test[] = allCombs.get(i);
			int ct=0;
			for (int k=0;k<sorteados.length;k++)
			{ 
				if (sorteados[k]==test[k])
					ct++;
			}

			if (ct>=garantir)
			{
				return true;
			}
		}

		return found;

	}


	private void printCombs()
	{
		for (int i=0;i<allCombs.size();i++)
		{
			System.out.println((i+1)+":"+Arrays.toString(allCombs.get(i)));
		}
	}



	private void testAll(boolean print)
	{
		stopTesting = false;
		testNum=0;
		allCombinations(arr, size, 0, new int[size],print);
	}



	int failedTests=0;
	private void allCombinations(int[] arr, int len, int startPosition, int[] result,boolean printFailed){
		if (stopTesting) return;
		if (len == 0){
			if (!test(result,garantir))
			{
				maxGoodTestNum--;
				failedTests++;
				if (printFailed)
					System.out.println(failedTests+"=="+Arrays.toString(result)+" failed");
				stopTesting=true;
				
			}
			else
			{
				maxGoodTestNum=testNum;
			}
			return;
		}       
		for (int i = startPosition; i <= arr.length-len; i++){
			result[result.length - len] = arr[i];
			
			allCombinations(arr, len-1, i+1, result,printFailed);
			if (stopTesting) return;
		}
	}


	private void addNumber()
	{
		int[] result = new int[size];

		for (int i=0;i<result.length;i++)
		{

			int num = numberPool.getNumber(result);
			if (num==-1) return;
			result[i] = num;
		}
		numberPool.reorder();
		//test if game is already added
		//System.out.println("adding "+Arrays.toString(result)+" size:"+allCombs.size());
		
		//if (!checkIfAlreadyInResult(result))
		Arrays.sort(result);
				allCombs.add(result);


	}
 


	public void setGoodCombinationListener(GoodCombinationListener li)
	{
		this.listener=li;
	}



}