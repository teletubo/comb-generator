package xnum;

import java.util.Arrays;
import java.util.LinkedList;

import javax.xml.bind.ValidationException;

public class CombinationsTester {



	//int[] arr = {1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21,22,23,24,25,26,27,28,29,30};
	int[] arr = {1,2,3,4,5,6,7,8,9,10,11,12};  
	 int garantir=4;


	int size = 6;
	LinkedList<int[]> allCombs = new LinkedList<int[]>();

	public CombinationsTester(int[] arr, int garantir, int size, LinkedList<int[]> allCombs) {
		super();
		this.arr = arr;
		this.garantir = garantir;
		this.size = size;
		this.allCombs = allCombs;
	}


	 
 
 




	
	static int testNum=0;

	
	private boolean testCt(int[] sorteados, int garantir)
	{

		testNum++;
		boolean found=false;
		for (int i=0;i<allCombs.size();i++)
		{
			int test[] = allCombs.get(i);
			int ct=0;
			for (int k=0;k<sorteados.length;k++)
			{
				for (int j=0;j<sorteados.length;j++)
					if (sorteados[k]==test[j])
						ct++;
			}

			if (ct>garantir)
			{ 
				found=true;
			}
		}
		 
		return found;

	}
	
	private boolean test(int[] sorteados, int garantir)
	{

		testNum++;
		boolean found=false;
		for (int i=0;i<allCombs.size();i++)
		{
			int test[] = allCombs.get(i);
			int ct=0;
			for (int k=0;k<sorteados.length;k++)
			{
				for (int j=0;j<sorteados.length;j++)
					if (sorteados[k]==test[j])
						ct++;
			}

			if (ct>=garantir)
			{
				
				//System.out.println(testNum+":Achou result("+ct+"): "+Arrays.toString(test));
				found=true;
			}
		}
		 
		return found;

	}


	private void printCombs()
	{
		for (int i=0;i<allCombs.size();i++)
		{
			System.out.println((i+1)+":"+Arrays.toString(allCombs.get(i)));
		}
	}



	public void testAll() throws ValidationException
	{
		allCombinations(arr, size, 0, new int[size]);
	}



	int failedTests=0;
	private void allCombinations(int[] arr, int len, int startPosition, int[] result) throws ValidationException{
		if (len == 0){
			if (!test(result,garantir))
			{
				failedTests++;
				throw new ValidationException(""+Arrays.toString(result)+" did not pass.");
			}
			return;
		}       
		for (int i = startPosition; i <= arr.length-len; i++){
			result[result.length - len] = arr[i];
			allCombinations(arr, len-1, i+1, result);
		}
	}
	
 
	 
	
	
	 


}