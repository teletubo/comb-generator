package xnum;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.InputStreamReader;

public class UnfoldSaver { 
	String fileName;
	
	public static void main(String[] args) throws Exception {
		
		
		
		File folder = new File(args[0]);
		File[] listOfFiles = folder.listFiles();

		    for (int i = 0; i < listOfFiles.length; i++) {
		      if (listOfFiles[i].isFile()) {
		    	  
		    	  
		    	  if (!listOfFiles[i].getName().endsWith("txt")) continue;
		  		UnfoldSaver ufs = new UnfoldSaver();
		  		System.out.println("processing "+listOfFiles[i]);
				String filename = ufs.parseFile(listOfFiles[i].getAbsolutePath());
				
				
				CmbLoaderTest cmb = new CmbLoaderTest();
				cmb.load(filename);  
//				cmb.test();
				
		    	   
		      } else if (listOfFiles[i].isDirectory()) {
		        System.out.println("Directory " + listOfFiles[i].getName());
		      }
		    }

		
		
		
		
	}
	
	private void generateOutputFilename(String filePath)
	{
		File f = new File(filePath);
		
		String filename = f.getName();
		
		String sDrawSize = filename.substring(0,1);
		
		String sGuarantees = filename.substring(1,2);
		
		String sNumbers = filename.substring(10,12);

		System.out.println("drawSize:"+sDrawSize+"  -  Guarantees:"+sGuarantees+"  -  Numbers: "+sNumbers +" qty:");
		
		
		int numbers = Integer.parseInt(sNumbers);
		
		int guarantees = Integer.parseInt(sGuarantees);
		
		
		int drawSize = Integer.parseInt(sDrawSize);
		
		fileName = String.format("%02d", numbers) + "_" + String.format("%02d", drawSize) + "_"+String.format("%02d", guarantees)+".cmb";
		
		System.out.println("filename: "+fileName);
	 
		
		
		
		
	}
	
	public void loadFile(String filePath)
	{
		
	}
	
	public String parseFile(String filePath) throws Exception 
	{
		generateOutputFilename(filePath);
		FileInputStream fstream = new FileInputStream(filePath);
		
		
		FileOutputStream fos = new FileOutputStream(fileName);
		
		
		BufferedReader br = new BufferedReader(new InputStreamReader(fstream));

		String strLine;
 
	
		while ((strLine = br.readLine()) != null)   { 
		  
		  if (strLine.length()>0)
		  {
			  String []nums  = strLine.trim().split(" ");
			  
			  if (nums.length<3)
				  nums = strLine.trim().split(".");
			  
			  for (int i=0;i<nums.length;i++)
			  {
				  if (nums[i].length()==0) continue;
				  fos.write(Integer.parseInt(nums[i]));
				  
				  //System.out.print(Integer.parseInt(nums[i])+" ");
			  }
			  
			 // System.out.println();
			  
			  
		  }
		}
		fos.close();
		br.close();
		
		return fileName;
		
		
	}
}
