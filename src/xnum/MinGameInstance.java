package xnum;

public class MinGameInstance {

	
	public int numbers[];
	public boolean covered=false;
	
	
	@Override
	public boolean equals(Object mgi)
	{
		
		 for (int i=0;i<numbers.length;i++)
			 if (numbers[i]!=((MinGameInstance)mgi).numbers[i]) return false;
		 
		 
		 return true;
		
	}
	
	
	@Override
	public int hashCode()
	{
		return numbers[0];
	}
}
