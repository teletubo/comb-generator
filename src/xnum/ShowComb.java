package xnum;

import java.util.Arrays;

public class ShowComb {
    public static void main(String[] args){
        String[] arr = {"A","B","C","D","E","F","A","B","C","D","E","F","A","B","C","D","E","F","a","b"};
        int size = 4;
        combinations2(arr, size, 0, new String[size]);
    }

    static int ct=0;
    static void combinations2(String[] arr, int len, int startPosition, String[] result){
        if (len == 0){
        	ct++;
            System.out.println(ct+":"+Arrays.toString(result));
            return;
        }       
        for (int i = startPosition; i <= arr.length-len; i++){
            result[result.length - len] = arr[i];
            combinations2(arr, len-1, i+1, result);
        }
    }       
}