package xnum;

import java.util.LinkedList;

public class GameUtils {

	LinkedList<int[]> allGames  = new LinkedList<>();
	
	private void createallCombinations(int[] arr, int len, int startPosition, int[] result){
		if (len == 0){ 

			allGames.add(result.clone());
			 
			return;
		}       
		for (int i = startPosition; i <= arr.length-len; i++){
			result[result.length - len] = arr[i];
			createallCombinations(arr, len-1, i+1, result);
		}
	}
	
	
	public LinkedList<int[]>  getAllGames(int size, int[] universe)
	{
		createallCombinations(universe, size, 0, new int[size]);
		return allGames;
	}
	
	
	
	private void createallCombinationsCallback(int[] arr, int len, int startPosition, int[] result,GameFoundInterface callback){
		if (len == 0){ 

		  callback.gameFound(result);
			 
			return;
		}       
		for (int i = startPosition; i <= arr.length-len; i++){
			result[result.length - len] = arr[i];
			createallCombinationsCallback(arr, len-1, i+1, result,callback);
		}
	}
	
	
	public void  getAllGamesCallback(int size, int[] universe,GameFoundInterface callback)
	{
		createallCombinationsCallback(universe, size, 0, new int[size],callback);
		 
	}
	
	
}
