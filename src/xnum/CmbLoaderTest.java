package xnum;

import java.io.File;
import java.io.FileInputStream;
import java.util.Arrays;
import java.util.LinkedList;

import javax.xml.bind.ValidationException;

public class CmbLoaderTest {
	
	public int numbers, draws, guarantees;
	String fileName;
	File file;
	
	public static void main(String[] args) {
		
		
		
		CmbLoaderTest cmb = new CmbLoaderTest();
		cmb.load("20_06_04.cmb");
		cmb.printResults();
		
		cmb.test();
		
	}
	
	
	public LinkedList<int[]> allCombs = new LinkedList<>();
	
	
	public void load(String filename )
	{
		load(filename, null);
	}
	public File load(String filename,String path)
	{
		
		this.fileName=filename;
		  numbers = Integer.parseInt(filename.substring(0,2));
		
		  draws = Integer.parseInt(filename.substring(3,5));
		  
		  guarantees =    Integer.parseInt(filename.substring(6,8));
		
		
		System.out.println("numbers:"+numbers+"      draws:"+draws+"   guarantees:"+guarantees);
		
		
		File file = null;
		
		if (path ==null)
			file = new File(filename);
		else
			file = new File(path+"/"+filename);
		
		byte bytes[] = null;
		
		try{
			bytes=readContentIntoByteArray(file);
		}catch (Exception e)
		{
			e.printStackTrace();
			return file;
		}
		
		
		int ct = 0;
		
		while (true)
		{
			int nums[] = new int[draws];
			
			
			for (int i=0;i<draws;i++)
			{
				if (ct == bytes.length)
				{
					System.out.println("pem");
					break;
				}
				nums[i] = bytes[ct];
				ct++;
			} 
			allCombs.add(nums);
			
			if (ct >= bytes.length-1)
			{
				break;
			}
		}
		 
		return file;
		
		 
	}
	
	
	public void test()
	{
	
		int universe[] = new int[numbers];
		
		
		for (int i=0;i<universe.length;i++) universe[i] = i+1;
		
		boolean passed=false;
		
		CombinationsTester tester = new CombinationsTester(universe, guarantees, draws, allCombs);
		try {
			tester.testAll();
			passed=true;
		} catch (ValidationException e) {
			e.printStackTrace();
			System.out.println(fileName+" failed");
		}
		if (passed)
		System.out.println(fileName+" passed");
	}
	
	
	private void printResults()
	{
		for (int i=0;i<allCombs.size();i++)
		{
			int nums[] = allCombs.get(i);
			
			System.out.println(i+")"+Arrays.toString(nums));
		}
	}

private static byte[] readContentIntoByteArray(File file) throws Exception
{
   FileInputStream fileInputStream = null;
   byte[] bFile = new byte[(int) file.length()];
  
   {
      //convert file into array of bytes
      fileInputStream = new FileInputStream(file);
      fileInputStream.read(bFile);
      fileInputStream.close();
 
   } 
   return bFile;
}
}
