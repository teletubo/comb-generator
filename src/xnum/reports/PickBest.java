package xnum.reports;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.StandardCopyOption;
import java.util.Collections;
import java.util.LinkedList;

import xnum.CmbLoaderTest;

public class PickBest {

	LinkedList<CombinationDefinition> allCombDefinitions = new LinkedList<>();
	LinkedList<CombinationDefinition> bestCombs = new LinkedList<>();
	public static void main(String []args)
	{
		
	PickBest pb  = new PickBest();
	
	pb.extract();
	pb.compare();
	pb.printReport();
	pb.copyBest();
		
	}
	
	
	
	public void copyBest()
	{
		
		File theDir = new File("best"); 
		if (!theDir.exists()) {  
		    try{ theDir.mkdir();   } 
		    catch(SecurityException se){ }
		}
		
		for (int i =0;i<bestCombs.size();i++)
		{
			CombinationDefinition cd = bestCombs.get(i);
		try {
			Files.copy(cd.file.toPath(), new File("best/"+cd.filename).toPath(), StandardCopyOption.REPLACE_EXISTING);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		}
	}
	
	public void printReport()
	{
		Collections.sort(bestCombs);
		
		
		String leftAlignFormat = "| %-8d | %-8d | %-8d |%-6d | %-7s |%n";

		System.out.format("+----------+----------+----------+-------+---------+%n");
		System.out.format("| Numbers  | Poolsize | Garantir |  qty  | source  |%n");
		System.out.format("+----------+----------+----------+-------+---------+%n");
 
	 
		for (int i =0;i<bestCombs.size();i++)
		{
			CombinationDefinition cd = bestCombs.get(i);
			 System.out.format(leftAlignFormat, cd.numbers, cd.poolSize,cd.garantir,cd.qtyGames,cd.source);	
		}
		System.out.format("+----------+----------+----------+-------+---------+%n");
		
		
	}
	
	
	public void compare()
	{
		
		int drawSize = 15;
		int numSelectedMin = 17;
		int numSelectedMax = 24;
		
		int garantirMin = 11;
		int garantirMax = 14;
		
		for (int numbersSelected=numSelectedMin;numbersSelected<=numSelectedMax;numbersSelected++)
		{

			for (int garantir=garantirMin;garantir<=garantirMax;garantir++)
			{
				LinkedList<CombinationDefinition> thisCombs = new LinkedList<>();
				
				for (int i=0;i<allCombDefinitions.size();i++)
				{
					CombinationDefinition cd = allCombDefinitions.get(i);
					
					if (cd.garantir==garantir && cd.numbers == numbersSelected && cd.poolSize == drawSize)
					{
						thisCombs.add(cd);
					}
				}
				
				if (thisCombs.size()>0)
				{
					int minQty = 99999999;
					CombinationDefinition minCombDefinition=null;
					for (int i=0;i<thisCombs.size();i++)
					{
						CombinationDefinition cd = thisCombs.get(i);
						if (cd.qtyGames<minQty)
						{
							minCombDefinition = cd;
							minQty = cd.qtyGames;
						}
					}
					
					if (minCombDefinition!=null)
					{
						bestCombs.add(minCombDefinition);
					}
					
					
				}
				
				
			}
		}
	}
	
	
	public void extract()
	{
		
	
		File folder = new File(".");
		File[] listOfFiles = folder.listFiles();
		
		for (int i=0;i<listOfFiles.length;i++)
		{
			
			File ff = listOfFiles[i];
			if (ff.isDirectory() && ff.getName().startsWith("combs_"))
			{
				String source = ff.getName().split("_")[1];
				File[] combinationFiles = ff.listFiles();
				
				for (int k=0;k<combinationFiles.length;k++)
				{
					File cf = combinationFiles[k];
					if (cf.getName().endsWith("cmb"))
					{
					CmbLoaderTest cmb = new CmbLoaderTest(); 
					System.out.println("na:"+cf.getName()); 
					File f = cmb.load(cf.getName(),cf.getParent()+"/");
					
					CombinationDefinition cd = new CombinationDefinition();
					cd.filename  = cf.getName();
					cd.garantir = cmb.guarantees;
					cd.poolSize = cmb.draws;
					cd.numbers = cmb.numbers;
					cd.qtyGames = cmb.allCombs.size();
					cd.source = source;
					allCombDefinitions.add(cd);
					cd.file = f;
					
					 
					
					}
				}
				
				
			}
			
		}
		
	}
	
}
