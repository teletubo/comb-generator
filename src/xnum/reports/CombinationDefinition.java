package xnum.reports;

import java.io.File;

public class CombinationDefinition  implements Comparable<CombinationDefinition>{

	
	public int poolSize;
	public int garantir;
	public int numbers;
	public int qtyGames;
	public String filename;
	public String source;
	public File file;
	@Override
	public int compareTo(CombinationDefinition o) {
		if (this.numbers==o.numbers)
		{
			if (this.garantir> o.garantir) return 1;
			if (this.garantir< o.garantir) return -1;
			
		}
		
		if (this.numbers > o.numbers) return 1;
		
		if (this.numbers< o.numbers) return -1;
		
		return 0;
	}
	
}
